# hw01
# Project Title

One paragraph of project description goes here

## Math

It is possible to have math written with the LaTeX syntax.

Math written inside $``$ will be rendered inline with the text.

Math written inside triple back quotes, with the language declared as math, will be rendered on a separate line.

Example:

This math is inline $`a^2+b^2=c^2`$.

This is on a separate line
```math
a^2+b^2=c^2
```

## Emphasis

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with double **asterisks** or double __underscores__.

Combined emphasis, aka bold italics, with double **_asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

## Code and syntax highlighting

```matlab
% Set up grid
    N = 512;
    x = (2*pi/N)*(-N/2:N/2-1)';
	if exampleno ~= 3
		conserv(:, i) = conserv(:, i) ./ conserv0 - 1; % relative change
	end
    for step = 1:nsteps
        t = t + h;
	    v = etdrk4_kdv_step(v, g, E, E2, Q, f1, f2, f3);
    end
```
