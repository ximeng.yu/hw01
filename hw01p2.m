clear
clf

N = 19;
tic()
fff = hw01p2integral(N);
toc()

x = 1 : N;
plot(x,fff,'o-')
title('integral')
xlabel('N')
ylabel('F(N)')
grid on

N = 19;
fun = @(x,k) x .^ k .* exp(-x);
z = zeros(N,1);
tic()
for n = 1:N
    z(n) = integral(@(x) fun(x,n), 0, 1);
end
toc()

hold on
n=1:19; 
plot(n,z(n),'.-')
legend('numerics','analytic')


hold off