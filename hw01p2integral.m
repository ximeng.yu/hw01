function int = hw01p2integral(n)  
    int = ones (n,1);
    int(1) = 1 - 2 * exp(-1);
    for i = 2:n
        int(i) = i*int(i-1)-exp(-1);
        
    end

end



